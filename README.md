# jload

#### 介绍
用于动态加载js,css,html的插件

#### 快速使用

#### jload.version
##### 作用：获取当前版本

#### jload.js(src)
##### 作用：用于动态加载js文件
##### 参数：src:必须，string类型，js文件网络地址

#### jload.css(href)
##### 作用：用于动态加载css文件
##### 参数：href:必须，string类型，css文件网络地址

#### jload.url(options)
##### 作用：用于动态加载网络页面
##### 参数：options.url:必须，string类型，网络页面地址
##### options.container:必须，node类型，页面显示容器
##### options.part:选填，string类型，合理属性选择器，对应网络页面的节点
##### eg:
jload.url({
    url : '/index.html',
    container : document.body
});
加载index.html并在body内显示

jload.url({
    url : '/index.html',
    container : document.body,
    part : 'div'
});
加载index.html内所有div节点并在body内显示

;(function(root) {
	"use strict";
	
	var module = function() {
		this.version = 'v1.1.0';
	}
	module.prototype.js = function(url) {
		let scripts = document.scripts;
		let patt = new RegExp(url.substr(url.indexOf('/') + 1), 'i');
		let isLoaded = (()=>{
			let loaded = false;
			for(let i = 0; i < scripts.length; i++) {
				if(patt.test(scripts[i].getAttribute('src'))) {
					loaded = true;
					break;
				}
			}
			return loaded;;
		})();
		if(!isLoaded) {
			let script = document.createElement('script');
			script.src = url;
			document.documentElement.appendChild(script);
		}
	}
	module.prototype.css = function(url) {
		let links = document.links;
		let patt = new RegExp(url.substr(url.indexOf('/') + 1), 'i');
		let isLoaded = (()=>{
			let loaded = false;
			for(let i = 0; i < links.length; i++) {
				if(patt.test(links[i].getAttribute('href'))) {
					loaded = true;
					break;
				}
			}
			return loaded;;
		})();
		if(!isLoaded) {
			let link = document.createElement('link');
			link.rel = 'stylesheet';
			link.href = url;
			document.documentElement.appendChild(link);
		}
	}
	module.prototype.url = function(options) {
		let opitons = options || {}
		fetch(options.url)
		.then(response => {return response.text()})
		.then(data => {
			let old = options.container.querySelectorAll('script');
			if(options.part) {
				let div = document.createElement('div');
				div.style.display = 'none';
				div.innerHTML = data;
				options.container.appendChild(div);
				let html = '', emptyTag = new Array('meta', 'img', 'input', 'br');
				document.querySelectorAll(options.part).forEach(item => {
					let attrs = item.attributes, tag = item.nodeName.toLowerCase();
					html += '<' + tag;
					if(attrs.length != 0) {
						for(let i = 0; i < attrs.length; i++) {
							html += ` ${attrs[i].nodeName}="${attrs[i].nodeValue}"`;
						}
					}
					html += '>';
					if(!emptyTag.includes(tag)) html += `${item.innerHTML}</${tag}>`;
				});
				options.container.innerHTML = html;
			} else {
				options.container.innerHTML = data;
			}
			options.container.querySelectorAll('script').forEach(item => {
				let isOld = false;
				for(let i = 0; i < old.length; i++) {
					if(item.isSameNode(old[i])) {
						isOld = true;
						break;
					}
				}
				if(!isOld) {
					let attrs = item.attributes, script = document.createElement('script');
					for(let k = 0; k < attrs.length; k++) {
						script.setAttribute(attrs[k].nodeName, attrs[k].nodeValue);
					}
					script.innerHTML = item.innerHTML;
					options.container.removeChild(item);
					options.container.appendChild(script);
				}
			});
		});
	}
	root.jload= new module();
}(this));